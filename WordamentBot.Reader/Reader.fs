﻿[<RequireQualifiedAccess>]
module Wordament.Bot.Reader

    open System.Drawing

    let letters = [
        (0.0323666079153982, 0.0890882871334722, 0.0759493670886076, 'A');
        (0.0530363723762218, 0.0924531325108156, 0.065694600224323,  'B');
        (0.0326870693799071, 0.0751482134273354, 0.0317256849863804, 'C');
        (0.0616888319179619, 0.0898894407947444, 0.0607274475244352, 'D');
        (0.0272392244832559, 0.08684505688191, 0.0342893767024515,   'E');
        (0.0265983015542381, 0.0701810607274475, 0.0144207659029002, 'F');
        (0.0318859157186348, 0.0812369812530043, 0.0943759012978689, 'G');
        (0.0591251402018907, 0.0777119051434065, 0.0697003685306842, 'H');
        (0.0, 0.0576830636116007, 0.0,                               'I');
        (0.0363723762217593, 0.0884473642044544, 0.044864605031245,  'K');
        (0.0, 0.0730652139080276, 0.0243550713026759,                'L');
        (0.0942156705656145, 0.122095817977888, 0.0905303637237622,  'M');
        (0.0519147572504406, 0.0729049831757731, 0.0990225925332479, 'N');
        (0.0655343694920686, 0.085082518827111, 0.0679378304758853,  'O');
        (0.0605672167921807, 0.0797949046627143, 0.0177856112802436, 'P');
        (0.0589649094696363, 0.082198365646531, 0.0588046787373818,  'R');
        (0.0214709181220958, 0.0511136035891684, 0.0634513699727608, 'S');
        (0.026918763018747, 0.0576830636116007, 0.0,                 'T');
        (0.0533568338407307, 0.0805960583239865, 0.0640922929017786, 'U');
        (0.0564012177535651, 0.0698605992629386, 0.0400576830636116, 'V');
        (0.0858836724883833, 0.107675052074988, 0.106393206216952,   'W');
        (0.0536772953052395, 0.0645729850985419, 0.00400576830636116,'Y');
        (0.0647332158307963, 0.0948565934946323, 0.0294824547348181, 'Z')
    ]

    let isOrange (color:Color) =
        color.R > (byte)220 
        && color.G > (byte)130 
        && color.G < (byte)170
        && color.B < (byte)29

        
    let getBounds (bitmap:Bitmap) col row =
        let colUpper = seq { col .. 1 .. bitmap.Width-1 }
                        |> Seq.find (fun p -> not(isOrange (bitmap.GetPixel(p, row))))
        let rowUpper = seq { row .. 1 .. bitmap.Height-1 }
                        |> Seq.find (fun p -> not(isOrange (bitmap.GetPixel(col, p))))
        new Rectangle(col, row, colUpper-col, rowUpper-row)
        
    let getSquares (bitmap:Bitmap) startRow endRow = 
        let mutable row = startRow
        let mutable col = 0
        let mutable rectHeight = 1
        let mutable result = []

        while row < endRow do
            while col < bitmap.Width do
                let color = bitmap.GetPixel(col, row)
                if isOrange color then
                    let rect = getBounds bitmap col row
                    col <- col + rect.Width
                    rectHeight <- rect.Height
                    result <- rect :: result
                else
                    col <- col + 1
            col <- 0
            row <- row + rectHeight
            rectHeight <- 1

        result |> List.rev

    let findSquares (bitmap:System.Drawing.Bitmap) =
        let startRow = (int)(System.Math.Round((float bitmap.Height) * 0.14));
        let endRow = (int)(System.Math.Round((float bitmap.Height) * 0.7));
        getSquares bitmap startRow endRow
        
    let whitePixelCount (bitmap:Bitmap) =
        let isWhite pixel = 
            let p = bitmap.GetPixel pixel
            p.A > (byte)240 && p.R > (byte)245 && p.G > (byte)240
        let pixels = seq {
            for x in 0..bitmap.Width-1 do
                for y in 0..bitmap.Height-1 do
                    yield (x,y)
        }
        (float (pixels |> Seq.where isWhite |> Seq.length)) / (float (pixels |> Seq.length))

    let vDist (a:float,b,c) (aa,bb,cc) =
        sqrt ((a-aa)*(a-aa) +
              (b-bb)*(b-bb) +
              (c-cc)*(c-cc))

    let matchLetterVector (a,b,c) =
        letters |> List.minBy(fun v -> 
            let (aa, bb, cc, _) = v
            vDist (a,b,c) (aa,bb,cc))

    
    let toLetter (bitmap:Bitmap) (rect:Rectangle) =

        let w = rect.Width / 2;
        let h = rect.Height / 2;
        let size = new Size(w, h);
        let quads = seq {
            yield bitmap.Clone(new Rectangle(new Point(rect.X, rect.Y), size), bitmap.PixelFormat)
            yield bitmap.Clone(new Rectangle(new Point(rect.X + size.Width, rect.Y), size), bitmap.PixelFormat)
            yield bitmap.Clone(new Rectangle(new Point(rect.X, rect.Y + size.Height), size), bitmap.PixelFormat)
            yield bitmap.Clone(new Rectangle(new Point(rect.X + size.Width, rect.Y + size.Height), size), bitmap.PixelFormat)
        }
        let list = quads |> Seq.map (fun q -> whitePixelCount q) |> List.ofSeq
        let vector = (list.[1], list.[2], list.[3])
        let (a,b,c,letter) = matchLetterVector vector
        if (vDist (a,b,c) vector) > 0.02 then "*"
        else letter.ToString();

    let read bitmap =
        let squares = findSquares bitmap
        let letters = squares |> Seq.map (fun s -> toLetter bitmap s)
        letters

