﻿using System.Drawing;
using System.Web.Mvc;

namespace Wordament.Bot.Web.Controllers
{
    public class ReaderController : Controller
    {
        [System.Web.Http.HttpPost]
        public ActionResult Index()
        {
            var bitmap = (Bitmap)Image.FromStream(Request.InputStream);
            var letters = Reader.read(bitmap);

            var result = new ContentResult();
            result.Content = string.Concat(letters);
            return result;
        }
    }
}