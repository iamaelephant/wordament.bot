﻿using System.Web.Hosting;
using System.Web.Http;
using System.Web.Mvc;

namespace Wordament.Bot.Web.Controllers
{
    public class SolutionController : Controller
    {
        private static Trie.Trie trie;

        static SolutionController()
        {
            trie = Solver.loadTrie(HostingEnvironment.MapPath("~/App_Data/list.txt"));
        }

        public ActionResult Index([FromUri]string board)
        {
            var result = new JsonResult();
            result.JsonRequestBehavior = JsonRequestBehavior.AllowGet;
            result.Data = (Solver.solve(board, trie));
            return result;
        }
    }
}
